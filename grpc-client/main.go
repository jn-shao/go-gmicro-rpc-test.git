package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	//client "hello/proto/hello/grpc-client"
	client "hello/proto/pod/grpc-client"
)

func main() {
	//通过kong Grpc Web 代理
	//192.168.0.121:8000/hello.HelloService/SayHello
	//POST body
	//{
	//   "method":"SayHello",
	//   "path" :"SayHello",
	//   "header" : {"method":{"key":"method","values":["SayHello"]}},
	//   "get" :{"method":{"key":"method","values":["SayHello"]}},
	//   "post":{"method":{"key":"method","values":["SayHello"]}},
	//   "body" :"SayHello",
	//   "url":"SayHello"
	//}

	//conn, err := grpc.Dial("192.168.0.103:8082", grpc.WithInsecure())
	conn, err := grpc.Dial("192.168.0.115:8081", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	//c := client.NewHelloClient(conn)
	c := client.NewPodClient(conn)
	//str := "hello"
	id := int64(1)
	r, err := c.FindPodByID(context.Background(), &client.PodId{Id: id})
	if err != nil {
		panic(err)
	}
	fmt.Println(r.PodName)

}
