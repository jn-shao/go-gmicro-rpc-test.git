package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"hello/proto/hello"
	"time"
)

type Hello struct {
}

func (e *Hello) SayHello(ctx context.Context, req *hello.Request, rsp *hello.Response) error {
	fmt.Printf("%v  接受到 hello.SayHello 的请求 \r\n", time.Now())
	h, _ := json.Marshal(req.Header)
	g, _ := json.Marshal(req.Get)
	b2 := fmt.Sprintf("{success:'成功访问 /hello/SayHello Method:%v  Param:%s  Header:%s'}", req.Method, g, h)
	rsp.Body = b2
	return nil
}
func (e *Hello) LotsOfReplies(context.Context, *hello.Request, hello.Hello_LotsOfRepliesStream) error {
	return nil
}
func (e *Hello) LotsOfGreetings(context.Context, hello.Hello_LotsOfGreetingsStream) error {
	return nil
}
func (e *Hello) BidiHello(context.Context, hello.Hello_BidiHelloStream) error {
	return nil
}
