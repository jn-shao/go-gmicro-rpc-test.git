package main

import (
	"gitee.com/jn-shao/common"
	"github.com/asim/go-micro/plugins/server/grpc/v3"
	"github.com/asim/go-micro/v3"
	"github.com/asim/go-micro/v3/registry"
	"github.com/asim/go-micro/v3/server"
	"github.com/go-micro/plugins/v3/registry/consul"
	"hello/handler"
	"hello/proto/hello"
	"strconv"
)

var (
	//服务地址
	hostIp = "192.168.0.103"
	//服务地址
	serviceHost = hostIp
	//服务端口
	servicePort = "8082"

	//注册中心配置
	consulHost       = "192.168.0.121"
	consulPort int64 = 8500
)

func main() {
	//注册中心
	consul := consul.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{
			consulHost + ":" + strconv.FormatInt(consulPort, 10),
		}
	})

	//创建服务
	service := micro.NewService(
		//自定义服务地址，必须要写在其它参数前面
		micro.Server(grpc.NewServer(func(options *server.Options) {
			options.Advertise = serviceHost + ":" + servicePort
		})),

		//使用默认的micro rpc server
		//micro.Server(server.NewServer(func(options *server.Options) {
		//	options.Advertise = serviceHost + ":" + servicePort
		//})),
		micro.Name("go.micro.api.hello"),
		micro.Version("latest"),
		//指定服务端口
		micro.Address(":"+servicePort),

		//添加注册中心，
		micro.Registry(consul),
	)

	service.Init()

	//注册控制器
	if err := hello.RegisterHelloHandler(service.Server(), &handler.Hello{}); err != nil {
		common.Error(err)
	}
	// 启动服务
	if err := service.Run(); err != nil {
		common.Fatal(err)
	}
}
