# gitee.com/jn-shao/common v0.0.0-20221209121153-1536f4d64c84
## explicit; go 1.19
gitee.com/jn-shao/common
# github.com/Microsoft/go-winio v0.5.0
## explicit; go 1.12
github.com/Microsoft/go-winio
github.com/Microsoft/go-winio/pkg/guid
# github.com/ProtonMail/go-crypto v0.0.0-20210428141323-04723f9f07d7
## explicit; go 1.13
github.com/ProtonMail/go-crypto/bitcurves
github.com/ProtonMail/go-crypto/brainpool
github.com/ProtonMail/go-crypto/eax
github.com/ProtonMail/go-crypto/internal/byteutil
github.com/ProtonMail/go-crypto/ocb
github.com/ProtonMail/go-crypto/openpgp
github.com/ProtonMail/go-crypto/openpgp/aes/keywrap
github.com/ProtonMail/go-crypto/openpgp/armor
github.com/ProtonMail/go-crypto/openpgp/ecdh
github.com/ProtonMail/go-crypto/openpgp/elgamal
github.com/ProtonMail/go-crypto/openpgp/errors
github.com/ProtonMail/go-crypto/openpgp/internal/algorithm
github.com/ProtonMail/go-crypto/openpgp/internal/ecc
github.com/ProtonMail/go-crypto/openpgp/internal/encoding
github.com/ProtonMail/go-crypto/openpgp/packet
github.com/ProtonMail/go-crypto/openpgp/s2k
# github.com/acomagu/bufpipe v1.0.3
## explicit; go 1.12
github.com/acomagu/bufpipe
# github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
## explicit
github.com/alecthomas/template
github.com/alecthomas/template/parse
# github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf
## explicit
github.com/alecthomas/units
# github.com/armon/go-metrics v0.0.0-20180917152333-f0300d1749da
## explicit
github.com/armon/go-metrics
# github.com/asim/go-micro/plugins/client/grpc/v3 v3.0.0-20210630062103-c13bb07171bc
## explicit; go 1.16
github.com/asim/go-micro/plugins/client/grpc/v3
# github.com/asim/go-micro/plugins/config/source/consul/v3 v3.0.0-20210904061721-270d910b7328
## explicit; go 1.16
github.com/asim/go-micro/plugins/config/source/consul/v3
# github.com/asim/go-micro/plugins/server/grpc/v3 v3.7.0
## explicit; go 1.16
github.com/asim/go-micro/plugins/server/grpc/v3
# github.com/asim/go-micro/v3 v3.7.1
## explicit; go 1.16
github.com/asim/go-micro/v3
github.com/asim/go-micro/v3/api
github.com/asim/go-micro/v3/auth
github.com/asim/go-micro/v3/broker
github.com/asim/go-micro/v3/cache
github.com/asim/go-micro/v3/client
github.com/asim/go-micro/v3/cmd
github.com/asim/go-micro/v3/codec
github.com/asim/go-micro/v3/codec/bytes
github.com/asim/go-micro/v3/codec/grpc
github.com/asim/go-micro/v3/codec/json
github.com/asim/go-micro/v3/codec/jsonrpc
github.com/asim/go-micro/v3/codec/proto
github.com/asim/go-micro/v3/codec/protorpc
github.com/asim/go-micro/v3/config
github.com/asim/go-micro/v3/config/encoder
github.com/asim/go-micro/v3/config/encoder/json
github.com/asim/go-micro/v3/config/loader
github.com/asim/go-micro/v3/config/loader/memory
github.com/asim/go-micro/v3/config/reader
github.com/asim/go-micro/v3/config/reader/json
github.com/asim/go-micro/v3/config/source
github.com/asim/go-micro/v3/config/source/file
github.com/asim/go-micro/v3/debug/handler
github.com/asim/go-micro/v3/debug/log
github.com/asim/go-micro/v3/debug/profile
github.com/asim/go-micro/v3/debug/profile/http
github.com/asim/go-micro/v3/debug/profile/pprof
github.com/asim/go-micro/v3/debug/proto
github.com/asim/go-micro/v3/debug/stats
github.com/asim/go-micro/v3/debug/trace
github.com/asim/go-micro/v3/errors
github.com/asim/go-micro/v3/logger
github.com/asim/go-micro/v3/metadata
github.com/asim/go-micro/v3/plugins
github.com/asim/go-micro/v3/registry
github.com/asim/go-micro/v3/registry/cache
github.com/asim/go-micro/v3/runtime
github.com/asim/go-micro/v3/runtime/local/build
github.com/asim/go-micro/v3/runtime/local/git
github.com/asim/go-micro/v3/runtime/local/process
github.com/asim/go-micro/v3/runtime/local/process/os
github.com/asim/go-micro/v3/runtime/local/source
github.com/asim/go-micro/v3/selector
github.com/asim/go-micro/v3/server
github.com/asim/go-micro/v3/store
github.com/asim/go-micro/v3/transport
github.com/asim/go-micro/v3/util/addr
github.com/asim/go-micro/v3/util/backoff
github.com/asim/go-micro/v3/util/buf
github.com/asim/go-micro/v3/util/grpc
github.com/asim/go-micro/v3/util/mdns
github.com/asim/go-micro/v3/util/net
github.com/asim/go-micro/v3/util/pool
github.com/asim/go-micro/v3/util/registry
github.com/asim/go-micro/v3/util/ring
github.com/asim/go-micro/v3/util/signal
github.com/asim/go-micro/v3/util/socket
github.com/asim/go-micro/v3/util/tls
github.com/asim/go-micro/v3/util/wrapper
# github.com/beorn7/perks v1.0.1
## explicit; go 1.11
github.com/beorn7/perks/quantile
# github.com/bitly/go-simplejson v0.5.0
## explicit
github.com/bitly/go-simplejson
# github.com/cpuguy83/go-md2man/v2 v2.0.0
## explicit; go 1.12
github.com/cpuguy83/go-md2man/v2/md2man
# github.com/emirpasic/gods v1.12.0
## explicit
github.com/emirpasic/gods/containers
github.com/emirpasic/gods/lists
github.com/emirpasic/gods/lists/arraylist
github.com/emirpasic/gods/trees
github.com/emirpasic/gods/trees/binaryheap
github.com/emirpasic/gods/utils
# github.com/fatih/color v1.9.0
## explicit; go 1.13
github.com/fatih/color
# github.com/fsnotify/fsnotify v1.4.9
## explicit; go 1.13
github.com/fsnotify/fsnotify
# github.com/go-git/gcfg v1.5.0
## explicit
github.com/go-git/gcfg
github.com/go-git/gcfg/scanner
github.com/go-git/gcfg/token
github.com/go-git/gcfg/types
# github.com/go-git/go-billy/v5 v5.3.1
## explicit; go 1.13
github.com/go-git/go-billy/v5
github.com/go-git/go-billy/v5/helper/chroot
github.com/go-git/go-billy/v5/helper/polyfill
github.com/go-git/go-billy/v5/memfs
github.com/go-git/go-billy/v5/osfs
github.com/go-git/go-billy/v5/util
# github.com/go-git/go-git/v5 v5.4.2
## explicit; go 1.13
github.com/go-git/go-git/v5
github.com/go-git/go-git/v5/config
github.com/go-git/go-git/v5/internal/revision
github.com/go-git/go-git/v5/internal/url
github.com/go-git/go-git/v5/plumbing
github.com/go-git/go-git/v5/plumbing/cache
github.com/go-git/go-git/v5/plumbing/color
github.com/go-git/go-git/v5/plumbing/filemode
github.com/go-git/go-git/v5/plumbing/format/config
github.com/go-git/go-git/v5/plumbing/format/diff
github.com/go-git/go-git/v5/plumbing/format/gitignore
github.com/go-git/go-git/v5/plumbing/format/idxfile
github.com/go-git/go-git/v5/plumbing/format/index
github.com/go-git/go-git/v5/plumbing/format/objfile
github.com/go-git/go-git/v5/plumbing/format/packfile
github.com/go-git/go-git/v5/plumbing/format/pktline
github.com/go-git/go-git/v5/plumbing/object
github.com/go-git/go-git/v5/plumbing/protocol/packp
github.com/go-git/go-git/v5/plumbing/protocol/packp/capability
github.com/go-git/go-git/v5/plumbing/protocol/packp/sideband
github.com/go-git/go-git/v5/plumbing/revlist
github.com/go-git/go-git/v5/plumbing/storer
github.com/go-git/go-git/v5/plumbing/transport
github.com/go-git/go-git/v5/plumbing/transport/client
github.com/go-git/go-git/v5/plumbing/transport/file
github.com/go-git/go-git/v5/plumbing/transport/git
github.com/go-git/go-git/v5/plumbing/transport/http
github.com/go-git/go-git/v5/plumbing/transport/internal/common
github.com/go-git/go-git/v5/plumbing/transport/server
github.com/go-git/go-git/v5/plumbing/transport/ssh
github.com/go-git/go-git/v5/storage
github.com/go-git/go-git/v5/storage/filesystem
github.com/go-git/go-git/v5/storage/filesystem/dotgit
github.com/go-git/go-git/v5/storage/memory
github.com/go-git/go-git/v5/utils/binary
github.com/go-git/go-git/v5/utils/diff
github.com/go-git/go-git/v5/utils/ioutil
github.com/go-git/go-git/v5/utils/merkletrie
github.com/go-git/go-git/v5/utils/merkletrie/filesystem
github.com/go-git/go-git/v5/utils/merkletrie/index
github.com/go-git/go-git/v5/utils/merkletrie/internal/frame
github.com/go-git/go-git/v5/utils/merkletrie/noder
# github.com/go-micro/plugins/v3/registry/consul v1.1.0
## explicit; go 1.16
github.com/go-micro/plugins/v3/registry/consul
# github.com/golang/protobuf v1.5.2
## explicit; go 1.9
github.com/golang/protobuf/jsonpb
github.com/golang/protobuf/proto
github.com/golang/protobuf/ptypes
github.com/golang/protobuf/ptypes/any
github.com/golang/protobuf/ptypes/duration
github.com/golang/protobuf/ptypes/timestamp
# github.com/google/uuid v1.2.0
## explicit
github.com/google/uuid
# github.com/hashicorp/consul/api v1.9.0
## explicit; go 1.12
github.com/hashicorp/consul/api
github.com/hashicorp/consul/api/watch
# github.com/hashicorp/go-cleanhttp v0.5.1
## explicit
github.com/hashicorp/go-cleanhttp
# github.com/hashicorp/go-hclog v0.12.0
## explicit; go 1.13
github.com/hashicorp/go-hclog
# github.com/hashicorp/go-immutable-radix v1.0.0
## explicit
github.com/hashicorp/go-immutable-radix
# github.com/hashicorp/go-rootcerts v1.0.2
## explicit; go 1.12
github.com/hashicorp/go-rootcerts
# github.com/hashicorp/golang-lru v0.5.3
## explicit; go 1.12
github.com/hashicorp/golang-lru/simplelru
# github.com/hashicorp/serf v0.9.5
## explicit; go 1.12
github.com/hashicorp/serf/coordinate
# github.com/imdario/mergo v0.3.12
## explicit; go 1.13
github.com/imdario/mergo
# github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99
## explicit
github.com/jbenet/go-context/io
# github.com/kevinburke/ssh_config v0.0.0-20201106050909-4977a11b4351
## explicit
github.com/kevinburke/ssh_config
# github.com/mattn/go-colorable v0.1.8
## explicit; go 1.13
github.com/mattn/go-colorable
# github.com/mattn/go-isatty v0.0.12
## explicit; go 1.12
github.com/mattn/go-isatty
# github.com/matttproud/golang_protobuf_extensions v1.0.1
## explicit
github.com/matttproud/golang_protobuf_extensions/pbutil
# github.com/miekg/dns v1.1.43
## explicit; go 1.14
github.com/miekg/dns
# github.com/mitchellh/go-homedir v1.1.0
## explicit
github.com/mitchellh/go-homedir
# github.com/mitchellh/hashstructure v1.1.0
## explicit; go 1.14
github.com/mitchellh/hashstructure
# github.com/mitchellh/mapstructure v1.3.3
## explicit; go 1.14
github.com/mitchellh/mapstructure
# github.com/nxadm/tail v1.4.8
## explicit; go 1.13
github.com/nxadm/tail
github.com/nxadm/tail/ratelimiter
github.com/nxadm/tail/util
github.com/nxadm/tail/watch
github.com/nxadm/tail/winfile
# github.com/opentracing/opentracing-go v1.2.0
## explicit; go 1.14
github.com/opentracing/opentracing-go
github.com/opentracing/opentracing-go/ext
github.com/opentracing/opentracing-go/log
# github.com/oxtoacart/bpool v0.0.0-20190530202638-03653db5a59c
## explicit; go 1.12
github.com/oxtoacart/bpool
# github.com/patrickmn/go-cache v2.1.0+incompatible
## explicit
github.com/patrickmn/go-cache
# github.com/pkg/errors v0.9.1
## explicit
github.com/pkg/errors
# github.com/prometheus/client_golang v1.1.0
## explicit
github.com/prometheus/client_golang/prometheus
github.com/prometheus/client_golang/prometheus/internal
github.com/prometheus/client_golang/prometheus/promhttp
# github.com/prometheus/client_model v0.0.0-20190812154241-14fe0d1b01d4
## explicit; go 1.9
github.com/prometheus/client_model/go
# github.com/prometheus/common v0.6.0
## explicit
github.com/prometheus/common/expfmt
github.com/prometheus/common/internal/bitbucket.org/ww/goautoneg
github.com/prometheus/common/log
github.com/prometheus/common/model
# github.com/prometheus/procfs v0.0.5
## explicit
github.com/prometheus/procfs
github.com/prometheus/procfs/internal/fs
github.com/prometheus/procfs/internal/util
# github.com/russross/blackfriday/v2 v2.0.1
## explicit
github.com/russross/blackfriday/v2
# github.com/sergi/go-diff v1.1.0
## explicit; go 1.12
github.com/sergi/go-diff/diffmatchpatch
# github.com/shurcooL/sanitized_anchor_name v1.0.0
## explicit
github.com/shurcooL/sanitized_anchor_name
# github.com/sirupsen/logrus v1.7.0
## explicit; go 1.13
github.com/sirupsen/logrus
# github.com/uber/jaeger-client-go v2.29.1+incompatible
## explicit
github.com/uber/jaeger-client-go
github.com/uber/jaeger-client-go/config
github.com/uber/jaeger-client-go/internal/baggage
github.com/uber/jaeger-client-go/internal/baggage/remote
github.com/uber/jaeger-client-go/internal/reporterstats
github.com/uber/jaeger-client-go/internal/spanlog
github.com/uber/jaeger-client-go/internal/throttler
github.com/uber/jaeger-client-go/internal/throttler/remote
github.com/uber/jaeger-client-go/log
github.com/uber/jaeger-client-go/rpcmetrics
github.com/uber/jaeger-client-go/thrift
github.com/uber/jaeger-client-go/thrift-gen/agent
github.com/uber/jaeger-client-go/thrift-gen/baggage
github.com/uber/jaeger-client-go/thrift-gen/jaeger
github.com/uber/jaeger-client-go/thrift-gen/sampling
github.com/uber/jaeger-client-go/thrift-gen/zipkincore
github.com/uber/jaeger-client-go/transport
github.com/uber/jaeger-client-go/utils
# github.com/uber/jaeger-lib v2.4.1+incompatible
## explicit
github.com/uber/jaeger-lib/metrics
# github.com/urfave/cli/v2 v2.3.0
## explicit; go 1.11
github.com/urfave/cli/v2
# github.com/xanzy/ssh-agent v0.3.0
## explicit
github.com/xanzy/ssh-agent
# go.uber.org/atomic v1.7.0
## explicit; go 1.13
go.uber.org/atomic
# go.uber.org/multierr v1.1.0
## explicit
go.uber.org/multierr
# go.uber.org/zap v1.10.0
## explicit
go.uber.org/zap
go.uber.org/zap/buffer
go.uber.org/zap/internal/bufferpool
go.uber.org/zap/internal/color
go.uber.org/zap/internal/exit
go.uber.org/zap/zapcore
# golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
## explicit; go 1.17
golang.org/x/crypto/blowfish
golang.org/x/crypto/cast5
golang.org/x/crypto/chacha20
golang.org/x/crypto/curve25519
golang.org/x/crypto/curve25519/internal/field
golang.org/x/crypto/ed25519
golang.org/x/crypto/ed25519/internal/edwards25519
golang.org/x/crypto/internal/subtle
golang.org/x/crypto/poly1305
golang.org/x/crypto/ssh
golang.org/x/crypto/ssh/agent
golang.org/x/crypto/ssh/internal/bcrypt_pbkdf
golang.org/x/crypto/ssh/knownhosts
# golang.org/x/net v0.0.0-20210614182718-04defd469f4e
## explicit; go 1.17
golang.org/x/net/bpf
golang.org/x/net/context
golang.org/x/net/http/httpguts
golang.org/x/net/http2
golang.org/x/net/http2/h2c
golang.org/x/net/http2/hpack
golang.org/x/net/idna
golang.org/x/net/internal/iana
golang.org/x/net/internal/socket
golang.org/x/net/internal/socks
golang.org/x/net/internal/timeseries
golang.org/x/net/ipv4
golang.org/x/net/ipv6
golang.org/x/net/netutil
golang.org/x/net/proxy
golang.org/x/net/trace
# golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
## explicit
golang.org/x/sync/singleflight
# golang.org/x/sys v0.0.0-20210502180810-71e4cd670f79
## explicit; go 1.12
golang.org/x/sys/cpu
golang.org/x/sys/execabs
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
golang.org/x/sys/windows/registry
golang.org/x/sys/windows/svc/eventlog
# golang.org/x/text v0.3.6
## explicit; go 1.11
golang.org/x/text/secure/bidirule
golang.org/x/text/transform
golang.org/x/text/unicode/bidi
golang.org/x/text/unicode/norm
# google.golang.org/genproto v0.0.0-20200806141610-86f49bd18e98
## explicit; go 1.11
google.golang.org/genproto/googleapis/rpc/status
# google.golang.org/grpc v1.38.0
## explicit; go 1.11
google.golang.org/grpc
google.golang.org/grpc/attributes
google.golang.org/grpc/backoff
google.golang.org/grpc/balancer
google.golang.org/grpc/balancer/base
google.golang.org/grpc/balancer/grpclb/state
google.golang.org/grpc/balancer/roundrobin
google.golang.org/grpc/binarylog/grpc_binarylog_v1
google.golang.org/grpc/codes
google.golang.org/grpc/connectivity
google.golang.org/grpc/credentials
google.golang.org/grpc/encoding
google.golang.org/grpc/encoding/proto
google.golang.org/grpc/grpclog
google.golang.org/grpc/internal
google.golang.org/grpc/internal/backoff
google.golang.org/grpc/internal/balancerload
google.golang.org/grpc/internal/binarylog
google.golang.org/grpc/internal/buffer
google.golang.org/grpc/internal/channelz
google.golang.org/grpc/internal/credentials
google.golang.org/grpc/internal/envconfig
google.golang.org/grpc/internal/grpclog
google.golang.org/grpc/internal/grpcrand
google.golang.org/grpc/internal/grpcsync
google.golang.org/grpc/internal/grpcutil
google.golang.org/grpc/internal/metadata
google.golang.org/grpc/internal/resolver
google.golang.org/grpc/internal/resolver/dns
google.golang.org/grpc/internal/resolver/passthrough
google.golang.org/grpc/internal/resolver/unix
google.golang.org/grpc/internal/serviceconfig
google.golang.org/grpc/internal/status
google.golang.org/grpc/internal/syscall
google.golang.org/grpc/internal/transport
google.golang.org/grpc/internal/transport/networktype
google.golang.org/grpc/keepalive
google.golang.org/grpc/metadata
google.golang.org/grpc/peer
google.golang.org/grpc/resolver
google.golang.org/grpc/serviceconfig
google.golang.org/grpc/stats
google.golang.org/grpc/status
google.golang.org/grpc/tap
# google.golang.org/protobuf v1.28.1
## explicit; go 1.11
google.golang.org/protobuf/encoding/protojson
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/json
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genid
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/order
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protodesc
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/descriptorpb
google.golang.org/protobuf/types/known/anypb
google.golang.org/protobuf/types/known/durationpb
google.golang.org/protobuf/types/known/timestamppb
# gopkg.in/alecthomas/kingpin.v2 v2.2.6
## explicit
gopkg.in/alecthomas/kingpin.v2
# gopkg.in/natefinch/lumberjack.v2 v2.0.0
## explicit
gopkg.in/natefinch/lumberjack.v2
# gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7
## explicit
gopkg.in/tomb.v1
# gopkg.in/warnings.v0 v0.1.2
## explicit
gopkg.in/warnings.v0
