package main

import (
	"context"
	"fmt"
	"gitee.com/jn-shao/common"
	"github.com/asim/go-micro/plugins/client/grpc/v3"
	"github.com/asim/go-micro/v3"
	"github.com/asim/go-micro/v3/registry"
	"github.com/go-micro/plugins/v3/registry/consul"
	podSrv "hello/proto/pod"
	"strconv"
)

var (
	//注册中心配置
	consulHost       = "192.168.0.121"
	consulPort int64 = 8500
)

func main() {
	//注册中心
	consul := consul.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{
			consulHost + ":" + strconv.FormatInt(consulPort, 10),
		}
	})

	//创建服务
	service := micro.NewService(
		//添加注册中心，
		micro.Registry(consul),
		micro.Client(grpc.NewClient()),
	)

	service.Init()

	//c := hello.NewHelloService("go.micro.api.hello", service.Client())
	c := podSrv.NewPodService("go.micro.service.podSrv", service.Client())
	r, err := c.FindPodByID(context.Background(), &podSrv.PodId{
		Id: 1,
	})
	if err != nil {
		common.Error(err)
	}
	fmt.Println(r.PodName)
}
